#Tagpy quickstart (lets rename it to taggy)
----------------------

**TagEngine setup**

Tagpy setup is relatively simple. Create the fields for your data and then include them into a table. This is broken up intwo two main steps:
1) Table construction
```
from tagpy import Col, Table, TagTable, TagEngine


# Define item tables

col1 = Col("movie_name", "TEXT") # arg1 ->column name, arg2 ->datatype(Sqlite)
col2 = Col("year_released", int) # Some python datatypes are also supported (int, str, float)
col3 = Col("isbn", str, unique = True) # Constraints can be placed on columns as well

item_table = Table("movies")
item_table.add_cols(col1, col2, 3)

# Define tag tables
tag_name_col = Col("name", str)
tag_col2 = Col("type", str)
tag_table = tagpy.TagTable(tag_name_col) # TagTables must be given a column 
tag_table.add_cols(tag_col2)             # containing the tag name
```

2) TagEngine Initialization
```
# Continued from earlier

# Now choose a path to store your db and initialize the TagEngine.
te = TagEngine("data.sqlite3", item_table, tag_table)
te.setup_tables() # Initialize the TagEngine tables
```


With the TagEngine setup, all that's left is to populate the database with whatever needs tagging

```
# You can now add information into the system
>>> tag_1 = te.create_and_add_tag("drama", "genre") # first argument is tag_name, all other arguments other cols
>>> tag_2 =te.create_and_add_tag("tragedy", type = "genre") #keyword arguments work too1
>>> te.create_and_add_tag("action") # add_tag returns the tag if successful, -1 otherwise
<Tag object: (3, action, None)>

>>> te.get_tags()
[(1, "drama", "genre"), (2, "tragedy", "genre"), (3, "action", None)]

>>> item_1 = te.add_item(movie_name = "Cows from heaven", year_released = 1991)
>>> item_2 = te.add_item(movie_name = "Cows from heaven 2", year_released = 1994)

>>> te.tag_item(item_1, tag_1)
>>> te.tag_item_by_name(item_1, "action")
>>> te.tag_item_multi(item_1, [tag1, tag2])
```

Searching

```
# Continued from earlier

# Searching is a matter of using that unique tag_name.
>>> te.search_tags(["action", "drama"])
[<tagpy Item: Cows from heaven>, <tagpy Item: Cows from heaven 2>]
>>> te.search_tags(["action", "drama"], and_flag = True)
[<tagpy Item: Cows from heaven>]

# Tagpy handles string input too
>>> te.strsearch_tags("action genre:drama")
[<tagpy Item: Cows from heaven>, <tagpy Item: Cows from heaven 2>]

```