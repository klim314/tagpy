#!/usr/bin/env python3

"""
Tests for the event manager
"""
import unittest
import logging
from tagpy import Col, Table, TagTable, TagEngine


class CoreTest(unittest.TestCase):
	def __init__(self, *args):
		super().__init__(*args)
	
	def test_trivial(self):
		data  = 1
		data += 1
		assert(data == 2)
		assert(data == 2)
	
	def test_tag_engine_construction(self):
		table1 = Table("items")
		# check trivial cases with different datatypes
		col1 = Col("col1", str)
		col2 = Col("col2", str)
		col3 = Col("col3", int)
		# check if unique constraint works
		col4 = Col("col4", float, unique = True)
		table1.add_col(col1)
		table1.add_col(col2)
		table1.add_col(col3)
		table1.add_col(col4)

		table2 = Table("tags")
		tag1 = Col("col1", str)
		# check if foreign keys work
		tag2 = Col("col2", str, foreign_key = True, foreign_key_table = table1, foreign_key_col = col2)
		
		table2.add_col(tag1)
		table2.add_col(tag2)

		db = TagEngine("test.sqlite3", table1, table2)
		db.reset_tables()
		db.setup_tables()

		db.conn.commit()
		db.conn.close()
		del db

	def test_basic_tagging(self):
		item_table = Table("tag_test_1_items")
		col1 = Col("code", str)
		col2 = Col("name", str)
		col3 = Col("date", str)
		item_table.add_cols(col1, col2, col3)

		tag_table = TagTable("tag_test_1_tags")
		col1 = Col("name", str)
		col2 = Col("type", str)
		tag_table.add_cols(col1, col2)
		

		db = TagEngine("test.sqlite3", item_table, tag_table)
		db.reset_tables()
		db.setup_tables()

		# Create the items
		item1 = db.add_item("CODE-001", "thingy the thinging", "1-1-1111")
		item2 = db.add_item("CODE-002", "thingy the thinging 2", "1-1-2222")
		item3 = db.add_item("CODE-003", "thingy the thinging 3", "1-1-3333")
		
		# Create the tags
		tag1 = db.add_tag("theresa blather", "star", debug = 0)
		tag2 = db.add_tag("jonathan blather", "star", debug = 0)
		tag3 = db.add_tag("drama", "genre", debug = 0)
		tag4 = db.add_tag("action", "genre", debug = 0)

		# Test Tagging
		db.tag_item(item1[0], tag1[0])
		# Test repeated tagging
		db.tag_item(item1[0], tag1[0])
		db.tag_item_by_tag_name(item2[0], "theresa blather")
		db.tag_item_by_tag_name(item2[0], "drama")
		db.tag_item_by_tag_name(item2[0], "jonathan blather")
		
		# onecol(basic) tag and/or searches
		assert(db.search_tags_and(["theresa blather"]) == [(1, 'CODE-001', 'thingy the thinging', '1-1-1111'), (2, 'CODE-002', 'thingy the thinging 2', '1-1-2222')])
		assert(db.search_tags_and(["theresa blather", "ellie notreal"]) == [])
		assert(db.search_tags_or(["theresa blather", "ellie notreal"]) == [(1, 'CODE-001', 'thingy the thinging', '1-1-1111'), (2, 'CODE-002', 'thingy the thinging 2', '1-1-2222')])

		# twocol(genre) tag and/or searches
		twocol_or_search_1 = db.search_tags_field([
								("theresa blather", "star"), 
								("drama", "genre"), 
								("jonathan blather", "")
								], col2)
		assert(twocol_or_search_1 == [(1, 'CODE-001', 'thingy the thinging', '1-1-1111'), (2, 'CODE-002', 'thingy the thinging 2', '1-1-2222')])

		twocol_and_search_1 = db.search_tags_field([
			("theresa blather", "star"), 
			("drama", "genre"), 
			("jonathan blather", "")
			], col2, and_flag = True)
		print("TWOCOL1: ", twocol_and_search_1)
		assert(twocol_and_search_1 == [(2, 'CODE-002', 'thingy the thinging 2', '1-1-2222')])

		twocol_and_search_2 = db.search_tags_field([("theresa blather", "star"), ("drama", "gen"), ("jonathan blather", "")], col2, and_flag = True)
		assert(twocol_and_search_2 == [])


		db.conn.commit()
		db.conn.close()
		del db

		
	# 