#!/usr/bin/env python3
import sqlite3

__author__ = "Kenneth Lim"

class Col:
	"""
	Representation of a single column in a table. 
	Input
		name:
		datatype: string containing sqlite datatype or Python datatype
				  Currently, only str, int and float supported
		unique: bool. Apply UNIQUE constraint to this column
		not_null: bool. Apply NOT_NULL constraint to this column
		foreign_key: bool. Apply FOREIGN KEY constraint to this column
		foreign_key_table: Table object containing foreign key or string containing table name
		foreign_key_col: Col object containing column of interest or string containing column name
	"""
	def __init__(self, name, datatype, unique = False, 
		not_null = False, primary_key = False, autoincrement = False, nocase = False,
		foreign_key = False, foreign_key_table = None, foreign_key_col = None):
		# Make sure that a foreign key has a source
		if foreign_key:
			assert(foreign_key_table != None)
			assert(foreign_key_col != None)
		if autoincrement:
			assert(primary_key == True)
		# core information
		self.name = name
		self.table = None
		self.datatype = self.handle_datatype(datatype)

		#Constraints
		self.unique = unique
		self.not_null = not_null
		self.primary_key = primary_key
		self.autoincrement = autoincrement
		self.nocase = nocase
		

		# Foreign key. TRY TO HANDLE being passed objects
		self.foreign_key = foreign_key
		if type(foreign_key_table) is Table:
			self.foreign_key_table = foreign_key_table.name
		else:
			self.foreign_key_table = foreign_key_table
		if type(foreign_key_col) is Col:
			self.foreign_key_col = foreign_key_col.name
		else:
			self.foreign_key_col = foreign_key_col


	def handle_datatype(self, datatype):
		"""
		Converts a given datatype string or python object and converts it
		to a string containing aSQLite3 compatible datatype
		input:
			datatype: str or python object
		"""
		datatypes = {
		str: "TEXT",
		int: "INTEGER",
		float: "REAL",
		}
		if type(datatype) is str:
			return datatype
		elif type(datatype) is type:
			return datatypes[datatype]

	def create_constraint_string(self):
		"""
		Generates the SQL constraints string
		returns:
			string containing constraints
		"""
		constraints = []
		if self.unique:
			constraints.append("UNIQUE")
		if self.not_null:
			constraints.append("NOT NULL")
		if self.autoincrement:
			constraints.append("AUTOINCREMENT")
		if self.nocase:
			constraints.append("COLLATE NOCASE")
		return " ".join(constraints)

	def create_sql_text(self):
		"""
		Generates the SQL string for a single column
		"""
		text = "{} {} {}".format(self.name, self.datatype, self.create_constraint_string())
		return text

	def get(self, distinct = 0, **kwargs):
		pass


class Table:
	"""
	Representation of a SQLite table
	"""
	def __init__(self, name, has_id = True):
		# core data
		self.name = name
		self.initialized = 0
		self.cols = []
		self.col_set = set()
		self.pkeys = []
		if has_id:
			self.id_col = Col("_id", int, primary_key = True)
			self.add_col(self.id_col)
			self.id_name = "_id"
		else:
			self.id_name = None
		self.unique_groups = []

	def add_col(self, col):
		"""
		Add a given column to the table
		Column is stores in insertion order in cols. Column can also be accessed directly from the table
		e.g. tags.tag_names
		"""
		# Make sure no duplicate columns exist
		if col.name in self.col_set:
			print("Col (name) already in table")
			return -1
		self.cols.append(col)
		self.col_set.add(col.name)
		if col.primary_key:
			self.pkeys.append(col.name)
		col.table = self
		setattr(self, col.name, col)

	def add_cols(self, *args):
		# Add multiple columns
		for i in args:
			self.add_col(i)
	def set_unique_together(self, *cols):
		"""
		set specified columns as unique together
		"""
		pass

	def create_and_add_col(self):
		pass

	def create_sql_statement(self):
		"""
		Generate the raw SQL statement for the creation of the table
		"""
		#Handle multiple pkeys
		pkeys = []
		fkeys = []
		for col in self.cols:
			if col.primary_key:
				pkeys.append(col.name)
			if col.foreign_key:
				fkeys.append((col.name, col.foreign_key_table, col.foreign_key_col))
		col_text = ",\n".join([col.create_sql_text() for col in self.cols])
		if fkeys:
			fkey_texts = ["FOREIGN KEY({}) REFERENCES {}({})".format(*data) for data in fkeys]
			col_text += ",\n" + ",\n".join(fkey_texts)
		if pkeys:
			pkey_text = "PRIMARY KEY ({})".format(", ".join(pkeys))
			col_text += ",\n" + pkey_text
		col_text = ("""CREATE TABLE IF NOT EXISTS {}(\n"""
		"{}"
		");"
		).format(self.name, col_text)
		return col_text

	def get_col_names(self):
		"""
		returns:
			list of all column names for the table
		"""
		return [i.name for i in self.cols if i.name != "_id"]

	# ORM-like stuff goes below here		
	def get(self, distinct = 0, **kwargs):
		"""
		on an initialized table, performs a SELECT operation on the table. WHERE clauses are defined
		with key-dict_index pairs
		"""
		

class TagTable(Table):
	def __init__(self, name, has_id = True):
		super().__init__(name, has_id)
		self.tag_name_col = Col("name", str, unique = True, nocase = True)
		self.add_col(self.tag_name_col)

class TagTableRF(Table):
	def __init__(self, tag_name_col):
		self.tag_name_col = tag_name_col
		self.tag_name_col.unique = True
		self.tag_name_col.nocase = True
		self.add_col(self.tag_name_col)






class TagEngine:
	"""
	TagEngine Object
	Tagging system
	"""
	def __init__(self, db_path, item_table, tag_table = None):
		"""
		db_path: string. path to database
		item_table
		tag_table
		"""
		self.conn = sqlite3.connect(db_path)
		self.cur = self.conn.cursor()
		self.item_table = item_table
		if tag_table == None:
			temp = TagTable("tags")
			col1 = Col("type", str)
			temp.add_col(col1)
			self.tag_table = temp
		else:
			self.tag_table = tag_table

	def reset_tables(self):
		# Get the commands
		self.cur.execute("""
			select 'drop table ' || name || ';' from sqlite_master
    		where type = 'table' AND name != 'sqlite_sequence';
			""")
		data = [i[0] for i in self.cur.fetchall()]
		for datum in data:
			self.cur.execute(datum)

	def setup_tables(self):
		"""
		Create the tables defined in item_table and tag_table
		Also creates tag_item_map, the mapping of tags to items
		"""
		self.tag_item_map = """
			CREATE TABLE IF NOT EXISTS item_tags(
			tag_id INTEGER NOT NULL,
			item_id INTEGER NOT NULL,
			FOREIGN KEY (tag_id) references {}({}),
			FOREIGN KEY (item_id) references {}({}),
			PRIMARY KEY (tag_id, item_id)
			);
			""".format(self.tag_table.name, self.tag_table.id_name, self.item_table.name, self.item_table.id_name)
		self.cur.execute(self.item_table.create_sql_statement())
		self.item_table.initialized = 1
		self.cur.execute(self.tag_table.create_sql_statement())
		self.tag_table.initialized = 1
		self.cur.execute(self.tag_item_map)
		# self.tag_item_map.initialized  
		self.conn.commit()

	def add_item(self, *args, use_kwargs = 0, debug = 0,  **kwargs):
		"""
		Add an item with a given set of tags and a given set of values
		"""
		# add an item with cols(keys) and values (indices) 
		# into the item table
		arg_count = len(args)
		if arg_count == 0:
			print("No arguments provided")
			return 
		# Create the SQL query and add the item into the database
		cmd = """
			INSERT INTO {} ({})
			VALUES ({});
		"""
		param_fields = ", ".join(["?" for i in range(len(range(arg_count)))])

		cmd = cmd.format(
			self.item_table.name, 
			", ".join([i for i in self.item_table.get_col_names()]), 
			param_fields)

		if debug:
			print (cmd)
			print(args)

		self.cur.execute(cmd, args)
		# Collect the id of inserted item
		item_id = self.cur.lastrowid
		self.conn.commit()
		return (item_id, *args)

	def add_tag(self, tag_name, *args, debug = 0):
		"""
		add_tag(tag_name, *args, debug = 0)
		Creates the tag if doesn't exist, returns tuple (tag_id, tag_name, *args)
		If tag already exists, returns None
		Input
			tag_name: string containing the name of the tag
			*args
			debug:
		Returns: Tuple (tag_id, tag_name, *args) if tag does not exist, else None


		"""
		# Check if tag already exists
		cmd = """
		SELECT EXISTS(SELECT 1 FROM {} WHERE (name) = '{}' LIMIT 1);
		""".format(self.tag_table.name, tag_name)
		self.cur.execute(cmd)
		tag_exists = self.cur.fetchone()[0]
		if debug:
			print( "TAG EXISTS: ", tag_exists)
		# Create the tag if it doesn't exist
		if not tag_exists:
			col_names = self.tag_table.get_col_names()
			cmd = """
			INSERT INTO {} ({}) VALUES ({});
			""".format(
				self.tag_table.name, 
				", ".join(col_names), 
				", ".join(["?" for i in col_names]) )
			print(cmd)
			print(tag_name, *args)
			self.cur.execute(cmd, (tag_name, *args))
			tag_id = self.cur.lastrowid
			
		else:
			return None
		self.conn.commit()
		return (tag_id, tag_name,  *args)
	def get_tag(self, tag_name):
		cmd = """
		SELECT * from {}
			WHERE name = ?
		""".format(self.tag_table.name)
		self.cur.execute(cmd, [tag_name])
		return self.cur.fetchone()

	def tag_item(self, item_id, tag_id):
		cmd = """
		SELECT EXISTS(
			SELECT 1 FROM item_tags WHERE tag_id = ? AND item_id = ? LIMIT 1
			);
		"""
		self.cur.execute(cmd, (tag_id, item_id))
		exists_item_tag = self.cur.fetchone()[0]

		if exists_item_tag:
			return 0

		cmd = """
		INSERT INTO item_tags (tag_id, item_id)
		VALUES (?, ?);
		"""
		self.cur.execute(cmd, (tag_id, item_id))
		self.conn.commit()
		return 1

	def tag_item_by_tag_name(self, item_id, tag_name):
		tag = self.get_tag(tag_name)
		if not tag:
			raise Exception("No tag found with name '{}'".format(tag_name))
		tag_id = tag[0]
		return self.tag_item(item_id, tag_id)

	def get_item(self, ):
		pass
	def get_item_tag(self, item_id, add_col = None, term = None):
		"""
		Get tag info for an item 
		"""
		cmd  = """
			SELECT t.* from {} t, item_tags it 
			WHERE it.item_id = {} AND
			it.tag_id = t._id;
		""".format(self.tag_table.name, item_id)

		if add_col and term:
			if type(add_col) != str:
				add_col = add_col.name

			cmd  = """
				SELECT t.* from {} t, item_tags it 
				WHERE it.item_id = {} AND
				it.tag_id = t._id AND
				t.{} = '{}';
			""".format(self.tag_table.name, item_id, add_col, term)


		print(cmd)
		self.cur.execute(cmd)
		return(self.cur.fetchall())


	def get_item_tag_multi(self, item_ids):
		"""
		Given a list of item ids, identifies all tags associated with the item
		More optimized for bulk inserts. INCOMPLETE
		"""
		it_cmd = """
			SELECT * from item_tags it
			WHERE it.item_id = {}
		""".format(item_id)
		pass
		
	def get_all_tags(self):
		cmd = """
		SELECT * from {};
		""".format(self.tag_table.name)
		self.cur.execute(cmd)
		return self.cur.fetchall()

	def search_tags_and(self, tags, debug = 0):
		cmd = """
		SELECT DISTINCT i.* from {} i, {} t, item_tags it
		WHERE i._id = it.item_id AND t._id = it.tag_id AND t.name in ({})
		GROUP by i._id
		HAVING COUNT(*) = {};
		""".format(
			self.item_table.name,
			self.tag_table.name,
			", ".join(["'" + str(tag) + "'" for tag in tags]), 
			len(tags))
		if debug:
			print(cmd)
		self.cur.execute(cmd)
		return self.cur.fetchall()
	def search_tags_or(self, tags):
		cmd = """
			SELECT DISTINCT i.* from {} i, {} t, item_tags it 
			WHERE i._id = it.item_id AND t._id = it.tag_id AND t.name in ({});
		""".format(
			self.item_table.name,
			self.tag_table.name,
			", ".join(["'" + str(tag) + "'" for tag in tags]))
		self.cur.execute(cmd)
		return self.cur.fetchall()

	def search_tags(self, tags, and_flag = False):
		if and_flag:
			return self.search_tags_and(tags)
		else:
			return self.search_rags_or(tags)
	def search_tags_field(self, tag_data, col, and_flag = False):
		"""
		search_tags_field(tags, field_name, data)
		Performs a tag search including additional data from a column in the tag 
		table. Default behavior: OR
		tag_data: list of (tag, col_data tuples). tag is the tag name and coL_data the
				  tag associated data contained in col
		col: col object or string. Column object in tag table containing 
			 secondary information of interest or string containing said column's
			 name. "" for blank
		and_flag: bool. Use AND search instead of OR
		"""
		if type(col) is Col:
			col_name = col.name
		else:
			col_name = col
		categories = list(set([i[1] for i in tag_data]))
		# print("CATEGORIES")
		# print(categories)

		queries = []
		tag_dict = dict()
		# for each col_data, assign tags to them 
		for name, col_data in tag_data:
			if col_data in tag_dict:
				tag_dict[col_data].append(name)
			else:
				tag_dict[col_data] = [name]

		for category in categories:
			if category == "":
				for tag_name in tag_dict[category]:
					queries.append((
						"SELECT DISTINCT i.* from {} i, {} t, item_tags it "
						"WHERE i._id = it.item_id "
						"AND t._id = it.tag_id "
						"AND t.name = ('{}')"
				    ).format(
					self.item_table.name,
					self.tag_table.name,
					tag_name)
					)
			else:
				queries.append((
					"SELECT DISTINCT i.* from {} i, {} t, item_tags it "
					"WHERE i._id = it.item_id "
					"AND t._id = it.tag_id "
					"AND t.name in ({}) "
					"AND t.{} = '{}'"
			    ).format(
				self.item_table.name,
				self.tag_table.name,
				", ".join(["'{}'".format(tag_name) for tag_name in tag_dict[category]]),
				col_name,
				category
				))

		if and_flag:
			query = ("\nINTERSECT\n".join(queries))
		else:
			query = ("\nUNION\n".join(queries))
		# print(query)
		self.cur.execute(query)
		return self.cur.fetchall()

	def get_col_from_table(self, col, table, distinct = True):
		if not(type(col) is str):
			col = col.name
		if not(type(table) is str):
			table = table.name
		if distinct:
			distinct = "DISTINCT"
		else:
			distinct = ""

		cmd = """
			SELECT {} {} FROM {}
		""".format(distinct, col, table)
		self.cur.execute(cmd)
		return self.cur.fetchall()

class TagManager:
	def __init__(self, cur, conn):
		self.cur = cur
		self.conn = conn


if __name__ == "__main__":
	# Tagging test
	item_table = Table("tag_test_1_items")
	col1 = Col("code", str)
	col2 = Col("name", str, unique = True)
	col3 = Col("date", str)
	item_table.add_cols(col1, col2, col3)

	tag_table = TagTable("tag_test_1_tags")
	col1 = Col("name", str)
	col2 = Col("type", str)
	tag_table.add_cols(col1, col2)
	

	db = TagEngine("test.sqlite3", item_table, tag_table)
	db.reset_tables()
	db.setup_tables()

	item1 = db.add_item("CODE-001", "thingy the thinging", "1-1-1111")
	item2 = db.add_item("CODE-0021", "thingy the thinging 2", "1-1-2222")
	item3 = db.add_item("CODE-003", "thingy the thinging 3", "1-1-3333")
	
	tag1 = db.add_tag("theresa blather", "star", debug = 0)
	tag2 = db.add_tag("jonathan blather", "star", debug = 0)
	tag3 = db.add_tag("drama", "genre", debug = 0)
	tag4 = db.add_tag("action", "genre", debug = 0)
	
	print(db.get_all_tags())