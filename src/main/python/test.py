import tagpy

dbm = tagpy.DbManager("temp.sqlite3")
dbm.reset_tables()
dbm.setup_tables()
dbm.add_tag("t1", "tn1")
dbm.add_tag("t2", "tn2")
dbm.add_tag("t3", "tn3")
print("TEST")
dbm.add_item(["t1", "t2"],"i1", "in1")
s1 = dbm.search_tags_or(["t1", "t3"])
print(s1)
s1 = dbm.search_tags_and(["t1", "t3"])
print(s1)